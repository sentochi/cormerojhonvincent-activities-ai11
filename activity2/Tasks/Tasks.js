import { StyleSheet, Text, View } from 'react-native';

function Tasks() {
    return (
        <View style={styles.container}>
            <Text>Completed Tasks Go Here</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
});
export default Tasks;