import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './screens/Home'
import Tasks from './Tasks/Tasks'

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen name = "Home" component={Home} />
          <Tab.Screen name = "Tasks" component={Tasks} />
        </Tab.Navigator>
    </NavigationContainer>
  );
}