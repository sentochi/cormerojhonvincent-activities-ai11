import { useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, Button, Alert } from 'react-native';

export default function App() {
    const [text, setText] = useState("");

    const getValues = () => {
        Alert.alert("You just typed: ", text)
    }
  return (
    <SafeAreaView style={styles.container}>
        <TextInput style={styles.input} placeholder = "Enter text"
         onChangeText={(text) => setText(text)}/>
        <Button color="black" title="Submit" onPress={() => getValues()} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    width: 200,
    height: 75,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 1,
    padding: 5,
  },
});
