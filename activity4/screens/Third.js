import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";

function Third({ navigation }) {
    return (
      <View style={styles.view}>
        <ImageBackground
          source={require('../background/Third.png')}
          resizeMode="contain"
          style={styles.back}
        > 
          <Text style={styles.text}>I want to create games and stuff.
          </Text>
          <Ionic
            style={styles.buttons}
            name="ios-arrow-forward-circle"
            size={50}
            color={"white"}
            onPress={() => navigation.navigate("Fourth")}
          ></Ionic>
        </ImageBackground>
      </View>
    );
  }
  export default Third;

  const styles = StyleSheet.create({
    text: {
      alignItems: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      paddingTop: 40,
      fontStyle: 'italic',
    },
    buttons: {
      paddingTop: 400,
      paddingLeft: 160,
    },
    page: {
      fontWeight: "bold",
      fontSize: 20,
      color: "#212121",
      fontStyle:"italic"
    },
    view: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#569bb8",
    },
    back: {
      height: 700,
      width: 370,
      justifyContent: "center",
    },
  });