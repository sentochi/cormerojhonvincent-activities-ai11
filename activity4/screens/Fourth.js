import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";

function Fourth({ navigation }) {
    return (
      <View style={styles.view}>
        <ImageBackground
          source={require('../background/Fourth.png')}
          resizeMode="contain"
          style={styles.back}
        > 
            <Text style={styles.text}>I wanna learn about crypto.
            </Text>
          <Ionic
            style={styles.buttons}
            name="ios-arrow-forward-circle"
            size={50}
            color={"white"}
            onPress={() => navigation.navigate("Fifth")}
          ></Ionic>
        </ImageBackground>
      </View>
    );
  }
  export default Fourth;

  const styles = StyleSheet.create({
    text: {
      alignItems: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      paddingTop: 40,
      paddingLeft: 7,
      fontStyle: 'italic',
    },
    buttons: {
      paddingTop: 400,
      paddingLeft: 160,
    },
    page: {
      fontWeight: "bold",
      fontSize: 20,
      color: "#212121",
      fontStyle:"italic"
    },
    view: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#569bb8",
    },
    back: {
      height: 700,
      width: 370,
      justifyContent: "center",
    },
  });