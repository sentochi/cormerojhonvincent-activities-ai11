import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";

function Fifth({ navigation }) {
    return (
      <View style={styles.view}>
        <ImageBackground
          source={require('../background/Fifth.png')}
          resizeMode="contain"
          style={styles.back}
        >
          <Text style={styles.text}>For me, this course is much easier than other courses.
          </Text>
          <Ionic
            style={styles.buttons}
            name="ios-home-outline"
            size={50}
            color={"white"}
            onPress={() => navigation.navigate("Home")}
          ></Ionic>
        </ImageBackground>
      </View>
    );
  }
  export default Fifth;

  const styles = StyleSheet.create({
    text: {
      alignItems: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      paddingLeft: 10,
      paddingTop: 40,
      fontStyle: 'italic',
    },
    buttons: {
      paddingTop: 400,
      paddingLeft: 160,
    },
    page: {
      fontWeight: "bold",
      fontSize: 20,
      color: "#212121",
      fontStyle:"italic"
    },
    view: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#569bb8",
    },
    back: {
      height: 700,
      width: 370,
      justifyContent: "center",
    },
  });