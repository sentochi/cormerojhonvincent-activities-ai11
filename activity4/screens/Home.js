import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";

function Home({ navigation }) {
    return (
      <View style={styles.view}>
          
        <ImageBackground
          source={require('../background/Home.png')}
          resizeMode="contain"
          style={styles.back}
        >
            <Text style={styles.text}>Home</Text>
          <Ionic
            style={styles.buttons}
            name="ios-arrow-forward-circle"
            size={50}
            color={"white"}
            onPress={() => navigation.navigate("First")}
          ></Ionic>
        </ImageBackground>
      </View>
    );
  }
  export default Home;

  const styles = StyleSheet.create({
    text: {
      alignItems: 'center',
      justifyContent: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      paddingTop: 40,
      paddingLeft: 145,
      fontStyle: 'italic',
    },
    buttons: {
      paddingTop: 400,
      paddingLeft: 160,
    },
    page: {
      fontWeight: "bold",
      fontSize: 20,
      color: "#212121",
      fontStyle:"italic"
    },
    view: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#569bb8",
    },
    back: {
      height: 700,
      width: 370,
      justifyContent: "center",
    },
  });