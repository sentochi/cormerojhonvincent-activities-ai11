import { StyleSheet, Text,  View, ImageBackground } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";

function Second({ navigation }) {
    return (
      <View style={styles.view}>
        <ImageBackground
          source={require('../background/Second.png')}
          resizeMode="contain"
          style={styles.back}
        >
          <Text style={styles.text}>I also wanted to pursue I.T because of its potential
            to make a ton of money.
          </Text>
          <Ionic
            style={styles.buttons}
            name="ios-arrow-forward-circle"
            size={50}
            color={"white"}
            onPress={() => navigation.navigate("Third")}
          ></Ionic>
        </ImageBackground>
      </View>
    );
  }
  export default Second;

  const styles = StyleSheet.create({
    text: {
      alignItems: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      paddingTop: 40,
      fontStyle: 'italic',
      paddingHorizontal: 15,
    },
    buttons: {
      paddingTop: 400,
      paddingLeft: 160,
    },
    page: {
      fontWeight: "bold",
      fontSize: 20,
      color: "#212121",
      fontStyle:"italic"
    },
    view: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#569bb8",
    },
    back: {
      height: 700,
      width: 370,
      justifyContent: "center",
    },
  });