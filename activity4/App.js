import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HomeScreen from './screens/Home';
import First from './screens/First';
import Second from './screens/Second';
import Third from './screens/Third';
import Fourth from './screens/Fourth';
import Fifth from './screens/Fifth';

const Stack = createNativeStackNavigator();
export default function App({}) {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Home"
      >
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="First" component={First} />
        <Stack.Screen name="Second" component={Second} />
        <Stack.Screen name="Third" component={Third} />
        <Stack.Screen name="Fourth" component={Fourth} />
        <Stack.Screen name="Fifth" component={Fifth} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
